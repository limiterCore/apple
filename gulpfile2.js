var gulp = require("gulp");
var less = require("gulp-less");
var connect = require("gulp-connect");
var notifier = require('node-notifier');
var notify = require("gulp-notify");
var tinypng = require("gulp-tinypng");

gulp.task('compress', function() {
	gulp.src('img/animations/camera.png')
		.pipe(tinypng("oNPsjO6yX_HIXPGKZBcrglJU-_-xtqnb"))
		.pipe(gulp.dest('img/animations'));

});

gulp.task('connect', function() {
	connect.server({
		port: 1337,
		livereload: true,
		root: './'
	});
});

gulp.task('less', function() {
	gulp.src('css/main.less')
		.pipe(less())
			.on('error', function(err){
				notifier.notify({
					'title': 'My notification',
					'message': err.message
				});
				return false;
			})
		.pipe(notify("Всё заебись!"))
		.pipe(gulp.dest('css/'))
		.pipe(connect.reload())
});

gulp.task('reload', function() {
	gulp.src('css/main.less')
		.pipe(connect.reload());
})

gulp.task('watch', function() {
	gulp.watch('**/*.less', ['less']);
	gulp.watch('**/*.html', ['reload']);
});


gulp.task('default', ['connect', 'watch']);

