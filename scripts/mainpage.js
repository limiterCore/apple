$(document).ready(function() {
	// MAIN PAGE SCROLL

	var page = 0,
		centers = [537, 1726, 2926, 4126, 5326, 7000],
		windowHeight = $(window).height(),
		animationTime = 1000,
		quietPeriod = 100,
		lastAnimation = 0,
		fps = 16,
		frameTime = animationTime / fps;

	$(document).bind('mousewheel DOMMouseScroll MozMousePixelScroll', function(event) {
		event.preventDefault();
		var delta = event.originalEvent.wheelDelta || -event.originalEvent.detail;

		initScroll(event, delta);
	});

	// Initial position
	initPage();


	function initScroll(event, delta) {
		deltaOfInterest = delta;

		var $container = $(".mainpage"),
			marginTop;

		var timeNow = new Date().getTime();
		if(timeNow - lastAnimation < quietPeriod + animationTime) {
			event.preventDefault();
			return;
		}
		if (deltaOfInterest < 0) {
			if (page < centers.length - 1) {
				page++;
				marginTop = parseInt(centers[page] - windowHeight / 2);

				if (page === 5) {
					marginTop = $container.outerHeight() - (windowHeight - $(".header-wrapper").outerHeight());
				}

				marginTop = "-" + marginTop.toString() + "px";



				$container.css({
					"transform": "translate3d(0, "+marginTop+", 0)",
					"-moz-transform": "translate3d(0, "+marginTop+", 0)",
					"-webkit-transform": "translate3d(0, "+marginTop+", 0)",
				});

				// Start animations
				if (page === 1) {
					// Anim1
					var i = 0;
					var anim1 = setInterval(function() {
						i ++;
						if (i > 15) {
							clearInterval(anim1);
						}
						$(".anim1").addClass("frame_"+i);
					}, frameTime);
				}

				if (page === 2) {
					// Anim2
					var i = 0;
					setTimeout(function() {
						var anim2 = setInterval(function() {
							i ++;
							if (i > 7) {
								clearInterval(anim2);
							}
							$(".anim2").addClass("frame_"+i);
						}, frameTime);
					}, 500);
				}

				if (page === 3) {
					// Anim3
					var i = 0;
					setTimeout(function() {
						var anim3 = setInterval(function() {
							i ++;
							if (i > 3) {
								clearInterval(anim3);
							}
							$(".anim3").addClass("frame_"+i);
						}, frameTime);
					}, 800);
				}

				if (page === 4) {
					// Anim4
					var i = 0;
					setTimeout(function() {
						$(".anim4").addClass("anim4_active");
					}, 900);
				}
			}
		}
		else {
			if (page > 0) {
				page--;
				marginTop = parseInt(centers[page] - windowHeight / 2);
				//if (page == 0) {
				//	marginTop = 0;
				//}

				marginTop = "-" + marginTop.toString() + "px";
				$container.css({
					"transform": "translate3d(0, "+marginTop+", 0)",
					"-moz-transform": "translate3d(0, "+marginTop+", 0)",
					"-webkit-transform": "translate3d(0, "+marginTop+", 0)",
				});
			}
		}

		lastAnimation = timeNow;
	}

	function initPage() {
		var $container = $(".mainpage"),
			marginTop;


		marginTop = parseInt(centers[0] - windowHeight / 2);

		marginTop = "-" + marginTop.toString() + "px";

		$container.css({
			"transform": "translate3d(0, "+marginTop+", 0)",
			"-moz-transform": "translate3d(0, "+marginTop+", 0)",
			"-webkit-transform": "translate3d(0, "+marginTop+", 0)",
		});
	}



});