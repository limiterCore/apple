$(document).ready(function() {

	// Init slick
	$("#prod-slider").slick({
		slidesToShow: 1,
		centerMode: true,
		centerPadding: '500px',
		arrows: true,
	});

	$("#prod-slider").on("beforeChange", function() {
		var $slider = $(this),
			$next = $slider.find(".slick-next"),
			$prev = $slider.find(".slick-prev");

		$next.hide();
		$prev.hide();
	});

	$("#prod-slider").on("afterChange", function() {
		var $slider = $(this),
			$next = $slider.find(".slick-next"),
			$prev = $slider.find(".slick-prev");

		$next.show();
		$prev.show();
	});


	// // On before slide change
	// $('#prod-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
	//   alert(nextSlide);
	// });



	// POPUPS
	$(".popup-link").on("click", function() {
		var $t = $(this),
			href = $t.attr("data-href"),
			$w,
			effectIn = "bounceInDown";


		$.fancybox({
			autoWidth: true,
			autoHeight: true,
			fitToView : true,
			scrolling: false,
			autoSize : true,
			closeClick : false,
			openEffect : 'none',
			closeEffect : 'none',
			closeBtn : false,
			wrapCSS : 'form',
			padding : 0,
			openSpeed: 0,
			closeSpeed: 0,
			href: href
		});

		$w = $(".fancybox-wrap");

		$w.addClass("animated " + effectIn);
		$w.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
			$w.removeClass("animated " + effectIn);
		});

		return false;
	});

	$(".fancy-close").click(function(){
		$.fancybox.close();
		return false;
	});

	$(".slider-box").slick({
		slidesToShow: 1,
		centerMode: true,
		centerPadding: '389.5px',
		arrows: true,
		responsive: [
			{
				breakpoint: 1280,
				settings: {
					centerPadding: '260px',
				}
			},
			{
				breakpoint: 1024,
				settings: {
					centerPadding: '131px',
				}
			},
			{
				breakpoint: 768,
				settings: {
					centerMode: false
				}
			}
		]
	});

	$(".slider-box").on("beforeChange", function() {
		var $slider = $(this),
			$next = $slider.find(".slick-next"),
			$prev = $slider.find(".slick-prev");

		$next.hide();
		$prev.hide();
	});

	$(".slider-box").on("afterChange", function() {
		var $slider = $(this),
			$next = $slider.find(".slick-next"),
			$prev = $slider.find(".slick-prev");

		$next.show();
		$prev.show();
	});

	$('#open-1').click(function() {
		var $t = $(this);

		if ($t.hasClass("open")) {
			$('.nav__menu-sub_w160').hide();
			$t.removeClass("open");

		}
		else {
			$('.nav__menu-sub_w160').show();
			$t.addClass("open");
		}
	});

	$('#open-2').click(function() {
		var $t = $(this);

		if ($t.hasClass("open")) {
			$('.nav__menu-sub_w193').hide();
			$t.removeClass("open");

		}
		else {
			$('.nav__menu-sub_w193').show();
			$t.addClass("open");
		}
	});

	$('#open-3').click(function() {
		var $t = $(this);

		if ($t.hasClass("open")) {
			$('.nav__menu-sub_w239').hide();
			$t.removeClass("open");

		}
		else {
			$('.nav__menu-sub_w239').show();
			$t.addClass("open");
		}
	});

	$('#open-4').click(function() {
		var $t = $(this);

		if ($t.hasClass("open")) {
			$('.nav__menu-sub_w186').hide();
			$t.removeClass("open");

		}
		else {
			$('.nav__menu-sub_w186').show();
			$t.addClass("open");
		}
	});

	// HAMBURGER
	$('.open-menu-box').on('click', function(){
		$(this).toggleClass('open-menu-box_checked');
	});

	$('.open-menu-box').click(function() {
		var $t = $(this);

		if ($t.hasClass("open")) {
			$('.nav').hide();
			$t.removeClass("open");

		}
		else {
			$('.nav').show();
			$t.addClass("open");
		}
	});
});